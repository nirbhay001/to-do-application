function printDataAll(data) {
    deleteBtn.style.visibility = "hidden";
    list.innerHTML = '';
    data.forEach(element => {
        let node = document.createElement("li");
        if (element.checked === true) {
            node.innerHTML = `<div>
        <input id="${element.id}" type="checkbox" checked>
        <label for="${element.id}"><span>${element.text}</span></label>
        </div>`;
        }
        else {
            node.innerHTML = `<div>
        <input id="${element.id}" type="checkbox">
        <label for="${element.id}"><span>${element.text}</span></label>
        </div>`;
        }
        list.append(node);
    });
}

function printDataActive(dataComming) {
    let data = dataComming.filter((item) => {
        return item.checked == false;
    })
    list.innerHTML = '';
    data.forEach(element => {
        let node = document.createElement("li");
        node.innerHTML = `<div>
      <input id="${element.id}" type="checkbox" >
      <label for="${element.id}"><span>${element.text}</span></label>
      </div>`;
        list.append(node);
    });
    deleteBtn.style.visibility = "hidden";
}

function printCompletedData(dataComming) {
    let data = dataComming.filter((item) => {
        return item.checked === true;
    })
    list.innerHTML = '';
    data.forEach(element => {
        let node = document.createElement("li");
        node.innerHTML = `<div>
      <input id="${element.id}" type="checkbox"  checked>
      <label for="${element.id}"><span>${element.text}</span></label>
      </div>
      <span id="${element.id}" class="material-icons deleteBtn">
      delete
      </span>
      `
        list.append(node);
    })
    deleteItem();
}

function checked() {
    let checkBoxData = document.querySelectorAll("input[type='checkbox']");
    checkBoxData.forEach((item) => {
        item.addEventListener("click", () => {
            todos.forEach((toItem) => {
                if (item.id == toItem.id) {
                    toItem.checked = true;
                }
            })
        })
    })
}

function deleteItem() {
    let deleteBtn = document.querySelectorAll(".deleteBtn");
    deleteBtn.forEach((item) => {
        item.addEventListener("click", () => {
            todos.forEach((toItem, index) => {
                if (item.id == toItem.id) {
                    todos.splice(index, 1);
                    let data = todos.filter((item) => {
                        return item.checked == true;
                    })
                    printCompletedData(data);
                }
            })
        })
    })
}

function deleteCompleteData() {
    todos.forEach((item, index) => {
        if (item.checked === true) {
            todos.splice(index, 1);
            deleteCompleteData();
        }
    });
    printCompletedData(todos);
}
