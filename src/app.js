let inputTask = document.getElementById("inputId");
const all = document.getElementById("all");
const active = document.getElementById("active");
const completed = document.getElementById("completed");
let list = document.getElementById("ul_list");
let removeForm = document.getElementById("myForm");
let deleteBtn = document.getElementById("btn");
let id;

document.addEventListener('keypress', (event)=>{
  let keyCode = event.keyCode ? event.keyCode : event.which;
  if(keyCode === 13) {
    addTask();
  }
    
});

let todos = new Array();
todos = [];
function addTask() {
  let inputText = inputTask.value;
  if (inputText.length > 1) {
    let obj = {
      text: inputText,
      checked: false,
      id: new Date().getTime(),
    }
    inputTask.value = " ";
    todos.push(obj);
    list.innerHTML = '';
    if (id === 'active') {
      printDataActive(todos);
    }
    else {
      printDataAll(todos);
    }
    checked();
  }
}


all.addEventListener("click", () => {
  let idAll = document.getElementById("all");
  id = idAll.id;
  deleteBtn.style.visibility = "hidden";
  removeForm.style.display= 'block';
  all.classList.add("linkstyle");
  active.classList.remove("linkstyle");
  completed.classList.remove("linkstyle");
  printDataAll(todos);
  checked();
})

active.addEventListener("click", () => {
  let idActive = document.getElementById("active");
  id = idActive.id;
  deleteBtn.style.visibility = "hidden";
  removeForm.style.display = 'block';
  all.classList.remove("linkstyle");
  active.classList.add("linkstyle");
  completed.classList.remove("linkstyle");
  let activeData = todos.filter((item) => {
    return item.checked == false;
  })
  printDataAll(activeData);
  checked();
})

completed.addEventListener("click", () => {
  deleteBtn.style.visibility = "visible";
  removeForm.style.display = 'none';
  all.classList.remove("linkstyle");
  active.classList.remove("linkstyle");
  completed.classList.add("linkstyle");
  let completedData = todos.filter((item) => {
    return item.checked == true;
  })
  printCompletedData(completedData);
})
