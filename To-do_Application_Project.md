# To-do Application Project

## In this project, I have made a To-do Application using HTML, CSS and JavaScript.
* In this project, I am getting data from users and storing it in the object form and pushing it to the array. 
* In the user interface, there are three tabs.
    1. All.
    2. Active.
    3. Completed.
* In the 'All' tab, you can show all your input whether it is completed or active.
* In the 'Active' tab, you can show all your active tasks, which is not completed.
* In the 'Completed' tab, you can show all your completed tasks and also you can delete them either one by one or delete all at once.
* It is very easy to use in your day-to-day life to make things easier and complete tasks on time.

# Hosted Link
* https://jolly-unicorn-aaf45b.netlify.app/